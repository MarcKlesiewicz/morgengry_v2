﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Morgengry
{
    public class Amulet : Merchandise
    {
        public string Design { get; set; }
        public Level Quality { get; set; }

        //private static double lowQualityValue = 12.5;
        //private static double mediumQualityValue = 20.0;
        //private static double highQualityValue = 27.5;

        public static double LowQualityValue { get; set; } = 12.5;
        public static double MediumQualityValue { get; set; } = 20.0;
        public static double HighQualityValue { get; set; } = 27.5;

        public override double GetValue()
        {
            double value = 0;
            switch (Quality)
            {
                case Level.low:
                    value = LowQualityValue;
                    break;
                case Level.medium:
                    value = MediumQualityValue;
                    break;
                case Level.high:
                    value = HighQualityValue;
                    break;
            }
            return value;
        }



        public Amulet(string itemId, Level quailty, string design)//:
                                                                  // base(itemId)
        {
            ItemId = itemId;
            Quality = quailty;
            Design = design;
        }
        public Amulet(string itemId, Level quailty) :
            this(itemId, quailty, "")
        {
        }
        public Amulet(string itemId) :
            this(itemId, Level.medium, "")
        {
        }

        //public double GetValue()
        //{
        //    GetValue = Amulet
        //    switch (amulet.Quality)
        //    {
        //        case Level.low:
        //            return 12.5;
        //        case Level.medium:
        //            return 20.0;
        //        case Level.high:
        //            return 27.5;
        //        default:
        //            return 0;
        //    }
        //}


        public override string ToString()
        {
            return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";
        }
    }
}
