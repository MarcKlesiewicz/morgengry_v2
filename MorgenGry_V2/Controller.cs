﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Morgengry
{
    public class Controller : ValuableRepository
    {
        private ValuableRepository valuableRepo;

        public ValuableRepository ValuableRepo
        {
            get { return valuableRepo; }
            set { valuableRepo = value; }
        }

        public Controller()
        {

        }

        public void AddToList(IValuable valuable)
        {
            ValuableRepo.AddValuable(valuable);
        }




        //public MerchandiseRepository bookRepository = new MerchandiseRepository();
        //public MerchandiseRepository amuletRepository = new MerchandiseRepository();
        //public CourseRepository courseRepository = new CourseRepository();


        //public void AddToList(Merchandise merchandise) 
        //{
        //    if(merchandise is Book)
        //    {
        //        bookRepository.AddMerchandises(merchandise);
        //    }
        //    else if(merchandise is Amulet)
        //    {
        //        amuletRepository.AddMerchandises(merchandise);
        //    }
        //}
        //public void AddToList(Course course)
        //{
        //    courseRepository.AddCourse(course);
        //}
        //public Merchandise GetBook(string itemId)
        //{
        //    return bookRepository.GetMerchandise(itemId);
        //}
        //public Merchandise GetAmulet(string itemId)
        //{
        //    return amuletRepository.GetMerchandise(itemId);
        //}
        //public Course GetCourse(string name)
        //{
        //    return courseRepository.GetCourse(name);
        //}

    }

}
