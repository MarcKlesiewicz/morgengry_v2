﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Morgengry
{
    public class Course : IValuable
    {


        public static double CourseHourValue = 0;

        public string Name { get; set; }
        public int DurationInMinutes { get; set; }

        public Course(string name, int duration)
        {
            Name = name;
            DurationInMinutes = duration;
        }


        public Course(string name) :
            this(name, 0)
        { }

        public double GetValue()
        {
            if (DurationInMinutes % 60 == 0)
            {
                return DurationInMinutes / 60 * CourseHourValue;
            }
            else
            {
                return (DurationInMinutes / 60 + 1) * CourseHourValue;
            }
        }


        public override string ToString() => $"Name: {Name}, Duration in Minutes: {DurationInMinutes}, Pris pr påbegyndt time: {GetValue()}";
        //Does it work?
        //:)

    }
}
