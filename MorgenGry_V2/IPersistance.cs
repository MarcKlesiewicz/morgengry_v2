﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Morgengry
{
    public interface IPersistance
    {
        public void Save();

        public void Save(string fileName);

        public void Load();

        public void Load(string fileName);


    }
}
