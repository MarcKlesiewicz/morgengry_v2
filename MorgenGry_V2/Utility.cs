﻿using System;
using System.Collections.Generic;
using System.Text;
/*
namespace Morgengry
{
    public static class Utility
    {
       
        public static double GetValueOfBook(Book book)
        {
            return book.Price;
        }
        public static double GetValueOfAmulet(Amulet amulet)
        {
            switch (amulet.Quality)
            {
                case Level.low:
                    return 12.5;
                case Level.medium:
                    return 20.0;
                case Level.high:
                    return 27.5;
                default:
                    return 0;
            }
        }
        
        
        public static double LowQualityValue { get; set; }
        public static double MediumQualityValue { get; set; }
        public static double HighQualityValue { get; set; }
        public static double CourseHourValue { get; set; }

        static Utility()
        {
            LowQualityValue = 12.5;
            MediumQualityValue = 20;
            HighQualityValue = 27.5;
            CourseHourValue = 875;
        }

        public static double GetValueOfCourse(Course course)
        {
            if (course.DurationInMinutes % 60 == 0)
            {
                return course.DurationInMinutes / 60 * CourseHourValue;
            }
            else
            {
                return (course.DurationInMinutes / 60 + 1) * CourseHourValue;
            }
        }
        public static double GetValueOfMerchandise(Merchandise merchandise)
        {
            double value = 0;
            if(merchandise is Book)
            {
                Book book = merchandise as Book;
                value = book.Price;
            }
            else if(merchandise is Amulet)
            {
                Amulet amulet = merchandise as Amulet;
                switch (amulet.Quality)
                {
                    case Level.low:
                        value = LowQualityValue;
                        break;
                    case Level.medium:
                        value = MediumQualityValue;
                        break;
                    case Level.high:
                        value = HighQualityValue;
                        break;
                }

            }
            return value;
        }
        */
//    }
//}
    