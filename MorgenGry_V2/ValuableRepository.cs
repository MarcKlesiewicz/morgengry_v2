﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Morgengry
{
    public class ValuableRepository : IPersistance
    {

        private List<IValuable> valuables = new List<IValuable>();

        public List<IValuable> Valuables { get; set; }

        public void AddValuable(IValuable valuable)
        {
            valuables.Add(valuable);
        }

        public IValuable GetValuable(string id)
        {
            foreach (var item in valuables)
            {
                if (item is Amulet)
                {
                    Amulet tempAmulet = (Amulet)item;
                    if (id == tempAmulet.ItemId)
                    {
                        return item;
                    }
                }
                else if (item is Book)
                {
                    Book tempbook = (Book)item;
                    if (id == tempbook.ItemId)
                    {
                        return item;
                    }
                }
                else
                {
                    Course tempCourse = (Course)item;
                    if(id == tempCourse.Name)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public double GetTotalValue()
        {
            double total = 0;

            foreach (var item in valuables)
            {
                total += item.GetValue();
            }

            return total;
        }

        public int Count()
        {
            return valuables.Count;
        }

        public void Save()
        {
            string path = "ValuableRepository.txt";
           
            StreamWriter write = new StreamWriter(path);

                foreach (var item in valuables)
                {
                    if (item is Merchandise)
                    {
                        if (item is Amulet)
                        {
                            Amulet tempAmulet = (Amulet)item;

                            write.WriteLine($"Amulet;{tempAmulet.ItemId};{tempAmulet.Quality};{tempAmulet.Design}");

                        }
                        else
                        {
                            Book tempBook = (Book)item;

                            write.WriteLine($"Book;{tempBook.ItemId};{tempBook.Title};{tempBook.Price}");
             
                    }
                    }
                    else
                    {
                        Course tempCourse = (Course)item;
                        write.WriteLine($"Course;{tempCourse.Name};{tempCourse.DurationInMinutes}");
            
                    }
                

                }
                write.Close();
            
        }
        
        public void Save(string fileName)
            {

            StreamWriter write = new StreamWriter(fileName);

            foreach (var item in valuables)
            {
                if (item is Merchandise)
                {
                    if (item is Amulet)
                    {
                        Amulet tempAmulet = (Amulet)item;

                        write.WriteLine($"Amulet;{tempAmulet.ItemId};{tempAmulet.Quality};{tempAmulet.Design}");
                    }
                    else
                    {
                        Book tempBook = (Book)item;

                        write.WriteLine($"Book;{tempBook.ItemId};{tempBook.Title};{tempBook.Price}");
                    }
                }
                else
                {
                    Course tempCourse = (Course)item;
                    write.WriteLine($"Course;{tempCourse.Name};{tempCourse.DurationInMinutes}");

                }

            }
            write.Close();
        }


        public void Load()
        {

            StreamReader reader = new StreamReader("ValuableRepository.txt");

            while (!reader.EndOfStream)
            {
                var temp = reader.ReadLine().Split(";");
                switch (temp[0])
                {
                    case "Book":
                        if (double.TryParse(temp[3], out double Price))
                        {
                            valuables.Add(new Book(temp[1], temp[2], Price));
                        }

                        break;
                    case "Course":
                        if (int.TryParse(temp[2], out int duration))
                        {
                            valuables.Add(new Course(temp[1], duration));
                        }
                        break;
                    case "Amulet":
                        switch (temp[2])
                        {
                            case "low":
                                valuables.Add(new Amulet(temp[1], Level.low, temp[3]));
                                break;
                            case "high":
                                valuables.Add(new Amulet(temp[1], Level.high, temp[3]));
                                break;
                            default:
                                valuables.Add(new Amulet(temp[1], Level.medium, temp[3]));
                                break;
                        }
                        break;
                }
            }

            reader.Close();
        }

        public void Load(string fileName)
        {

            StreamReader reader = new StreamReader(fileName);

            while (!reader.EndOfStream)
            {
                var temp = reader.ReadLine().Split(";");
                switch (temp[0])
                {
                    case "Book":
                        if (double.TryParse(temp[3], out double Price))
                        {
                            valuables.Add(new Book(temp[1], temp[2], Price));
                        }

                        break;
                    case "Course":
                        if (int.TryParse(temp[2], out int duration))
                        {
                            valuables.Add(new Course(temp[1], duration));
                        }
                        break;
                    case "Amulet":
                        switch (temp[2])
                        {
                            case "low":
                                valuables.Add(new Amulet(temp[1], Level.low, temp[3]));
                                break;
                            case "high":
                                valuables.Add(new Amulet(temp[1], Level.high, temp[3]));
                                break;
                            default:
                                valuables.Add(new Amulet(temp[1], Level.medium, temp[3]));
                                break;
                        }
                        break;
                }
            }

            reader.Close();
        }
    }
}











