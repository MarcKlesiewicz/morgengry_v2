﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Morgengry;

//namespace UnitTestProject3
//{
//    [TestClass]
//    public class UnitTest4
//    {
//        Book b1, b2, b3;
//        Amulet a1, a2, a3;
//        Course c1, c2;



//        [TestInitialize]
//        public void Init()
//        {
//            // Arrange
//            b1 = new Book("1");
//            b2 = new Book("2", "Falling in Love with Yourself");
//            b3 = new Book("3", "Spirits in the Night", 123.55);

//            a1 = new Amulet("11");
//            a2 = new Amulet("12", Level.high);
//            a3 = new Amulet("13", Level.low, "Capricorn");

//            c1 = new Course("Eufori med røg");
//            c2 = new Course("Nuru Massage using Chia Oil", 157);

//            courses = new CourseRepository();
//            books = new MerchandiseRepository();
//            amulets = new MerchandiseRepository();

//            // Act
//            books.AddMerchandises(b1);
//            books.AddMerchandises(b2);
//            books.AddMerchandises(b3);

//            amulets.AddMerchandises(a1);
//            amulets.AddMerchandises(a2);
//            amulets.AddMerchandises(a3);

//            courses.AddCourse(c1);
//            courses.AddCourse(c2);
//        }

//        [TestMethod]
//        public void TestGetBook()
//        {
//            // Assert
//            Assert.AreEqual(b2, books.GetMerchandise("2"));
//        }
//        [TestMethod]
//        public void TestGetAmulet()
//        {
//            // Assert
//            Assert.AreEqual(a3, amulets.GetMerchandise("13"));
//        }
//        [TestMethod]
//        public void TestGetCourse()
//        {
//            // Assert
//            Assert.AreEqual(c1, courses.GetCourse("Eufori med røg"));
//        }
//        [TestMethod]
//        public void TestGetTotalValueForBook()
//        {
//            // Assert
//            Assert.AreEqual(123.55, books.GetTotalValue());
//        }
//        [TestMethod]
//        public void TestGetTotalValueForAmulet()
//        {
//            // Assert
//            Assert.AreEqual(60.0, amulets.GetTotalValue());
//        }
//        [TestMethod]
//        public void TestGetTotalValueForCourse()
//        {
//            // Assert
//            Assert.AreEqual(2625.0, courses.GetTotalValue());
//        }
//    }

//}
