﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MorgenGry_V2
{
    public class Utility
    {
        public static double GetValueOfBook(Book book)
        {
            double value = book.Price;
            return value;
        }

        public static double GetValueOfAmulet(Amulet amulet)
        {
            double value = 69.420;
            switch (amulet.Quality)
            {
                case Level.low:
                    value = 12.5;
                    break;

                case Level.medium:
                    value = 20.0;
                    break;
                case Level.high:
                    value = 27.5;
                    break;
            }
            return value;

        }

        public static double GetValueOfCourse(Course course)
        {
            int minute = course.DurationInMinutes;
            double value = 0;
            int count = 0;
            while (minute > 0)
            {
                while (count < 60 || minute != 0)
                {
                    count++;
                    minute--;
                }

                count = 0;
                value += 875;
            }

            return value;
        }
        

    }
}
